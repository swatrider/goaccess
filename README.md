# GoAccess
GoAccess Docker Container
## Usage
```
docker run --rm -it -v /var/log/nginx:/var/log/nginx swatrider/goaccess
goaccess -f /var/log/nginx/access.log

goaccess -f /var/log/nginx/access.log -o /etc/nginx/html/artemis/index.html --real-time-html --ws-url=staging.swatrider.com --port=80 --time-format %H:%M:%S --date-format %d/%b/%Y --log-format COMMON
```
